﻿using System;
using System.Collections.Generic;
using WzorceProjektowe.Patterns.Singleton;
using WzorceProjektowe.Patterns.Adapter;
using WzorceProjektowe.Patterns.Fasada;
using WzorceProjektowe.Patterns.Iterator;
using WzorceProjektowe.Patterns.Decorator;
using WzorceProjektowe.Patterns.Composite;
using WzorceProjektowe.Patterns.TemplateMethod;
using WzorceProjektowe.Patterns.Observer;
using WzorceProjektowe.Patterns.Proxy;
using WzorceProjektowe.Patterns.ChainOfResponsibility;
using WzorceProjektowe.Patterns.State;
using WzorceProjektowe.Patterns.Mediator;
using WzorceProjektowe.Patterns.Visitor;

namespace WzorceProjektowe
{
    class Program
    {
        static void Main(string[] args)
        {
            //Singleton
            var singleton = Singleton.Instance;
            var singleton2 = Singleton.Instance;

            //Adapter
            new List<Figura> { new Figura(), new Punkt(), new Linia(), new Okrag() }.ForEach(x => x.wyswietl());

            //Fasada
            var wek2d = new Wektor2D(1.2, 5.6);
            var wek3d = new Wektor3D(2.2, 3.6, 8.2);
            var wek3d2 = new Wektor3D(5.4, 6.2, 1.1);
            var sumaWek3d = wek3d + wek3d2;
            var sumaWek2d3d = wek2d + sumaWek3d;

            //Iterator
            var kolekcja = new MyCollection(new List<decimal> { 5.4m, 0m, 12.4m, 54.1m, 0, 12.1m });
            var iterator = kolekcja.GetIterator;
            while (iterator.IsDone())
                Console.WriteLine(iterator.CurrentItem);

            var iteratorNonZero = kolekcja.GetIteratorNonZero;
            while (iteratorNonZero.IsDone())
                Console.WriteLine(iteratorNonZero.CurrentItem);

            //Template method
            var connString = "Data Source=LUKASZK\\SQL2017EXPRESS; Initial Catalog=moj_sklep; user id=admin; password=admin123";
            var select = "SELECT * FROM Zamowienia WHERE Zamowienie.Id = 123";
            var sqlserver = new ZapytanieSQLServer();
            sqlserver.WykonajZapytanie(connString, select);
            var oracle = new ZapytanieOracle();
            oracle.WykonajZapytanie(connString, select);

            //Strategia
            var zamowienie = new Patterns.Strategia.Zamowienie(new List<double> { 245.45, 653.76, 875.34, 1224.65, 1289.99, 23455.89 });
            var konfiguracja = new Patterns.Strategia.Konfiguracja(Patterns.Strategia.TypPodatku.polski);
            zamowienie.obliczPodatek(konfiguracja);
            konfiguracja.podatek = Patterns.Strategia.TypPodatku.niemiecki;
            zamowienie.obliczPodatek(konfiguracja);

            //Most
            var prostokat1 = new Patterns.Most.Prostokat();
            prostokat1.Biblioteka = new Patterns.Most.BibliotekaV1();
            prostokat1.rysuj();
            var okrag1 = new Patterns.Most.Okrag();
            okrag1.Biblioteka = new Patterns.Most.BibliotekaV1();
            okrag1.rysuj();
            var prostokat2 = new Patterns.Most.Prostokat();
            prostokat2.Biblioteka = new Patterns.Most.BibliotekaV2();
            prostokat2.rysuj();
            var okrag2 = new Patterns.Most.Okrag();
            okrag2.Biblioteka = new Patterns.Most.BibliotekaV2();
            okrag2.rysuj();

            //Decorator
            var order = new Zamowienie();
            order.drukuj(Konfiguracja.TypPotwierdzenia.pierwsze);
            order.drukuj(Konfiguracja.TypPotwierdzenia.drugie);

            //Composite
            var linia = new Line();
            var prostokat = new Rectangle();
            var rootPicture = new Picture();
            rootPicture.Add(linia);
            rootPicture.Add(prostokat);
            var secondPicture = new Picture();
            secondPicture.Add(new Text());
            secondPicture.Add(linia);
            secondPicture.Add(prostokat);
            rootPicture.Add(secondPicture);
            rootPicture.Add(linia);
            rootPicture.Draw();

            //Abstract factory
            new Patterns.AbstractFactory.Konfiguracja(Patterns.AbstractFactory.Rozdzielczosc.niska);
            new Patterns.AbstractFactory.Konfiguracja(Patterns.AbstractFactory.Rozdzielczosc.wysoka);

            //Factory method
            var sqlServ = new Patterns.FactoryMethod.ZapytanieSQLServer();
            sqlServ.WykonajZapytanie(connString, select);
            var orac = new Patterns.FactoryMethod.ZapytanieOracle();
            orac.WykonajZapytanie(connString, select);

            //Observer
            var subj = new ConcreteSubject();
            subj.Attach(new ConcreteObserver(subj, "obs1"));
            subj.Attach(new ConcreteObserver(subj, "obs2"));
            subj.Attach(new ConcreteObserver(subj, "obs3"));

            subj.SubjectState = "Nowy status";
            subj.Notify();

            //Proxy
            var quadEqProxy = new QuadraticEquationProxy();
            quadEqProxy.CalculateQuadEq(5, 6, 1);
            quadEqProxy.CalculateQuadEq(5, 6, 1);

            //Chain of responsibility
            Handler aPrintButton = new PrintButton();
            Handler anOKButton = new OKButton();
            Handler aSaveDialog = new SaveDialog();
            Handler aPrintDialog = new PrintDialog();
            Handler anApplication = new Application();
            aPrintButton.SetSuccessor(aPrintDialog);
            anOKButton.SetSuccessor(aPrintDialog);
            aPrintDialog.SetSuccessor(anApplication);
            aSaveDialog.SetSuccessor(anApplication);
            aPrintButton.HandleHelp();
            anOKButton.HandleHelp();
            aSaveDialog.HandleHelp();

            //Builder
            var str = "A<b>l</b>a ma <i>k</i>o<u>t</u>a";
            var bldList = new List<Patterns.Builder.TextConverter> { new Patterns.Builder.Converter1(), new Patterns.Builder.Converter2(), new Patterns.Builder.Converter3(), new Patterns.Builder.Converter4() };
            var htmlReader = new Patterns.Builder.HTMLReader();
            foreach (var builder in bldList)
            {
                htmlReader.ParseHTML(str, new Patterns.Builder.Konfiguracja(builder));
                Console.WriteLine(builder.Text);
            }

            //State
            var file = new File();
            file.Open();
            file.Read();
            file.Close();
            file.Write();

            //Mediator
            Student student = new ConcreteStudent(new DialogWindow());
            student.wprowadzOceny();

            //Visitor
            var sklep = new SklepZoologiczny();
            sklep.Attach(new Ptak(true, 240.54d, 350.18d));
            sklep.Attach(new Gad(false, 240.99d, 600.78d));
            sklep.Attach(new Ssak(true, 120.45d, 300.23d));
            sklep.Attach(new Ryba(false, 101.10d, 200.12d));

            var cenySVisitor = new CenySklepoweVisitor();
            var cenyCVisitor = new CenyCzarnorynkoweVisitor();
            sklep.Accept(cenySVisitor);
            Console.WriteLine(cenySVisitor.suma);
            sklep.Accept(cenyCVisitor);
            Console.WriteLine(cenyCVisitor.suma);
            sklep.Accept(new WeterynarzVisitor());
        }
    }
}
