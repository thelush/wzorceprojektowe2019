﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Observer
{
    public class ConcreteSubject : Subject
    {
        public string SubjectState { get; set; }
    }
}
