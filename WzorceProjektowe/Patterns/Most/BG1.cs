﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Most
{
    public class BG1
    {
        public void rysuj_linie() => Console.WriteLine("Rysuję linię za pomocą biblioteki graficznej BG1");
        public void rysuj_okrag() => Console.WriteLine("Rysuję okrąg za pomocą biblioteki graficznej BG1");
    }
}
