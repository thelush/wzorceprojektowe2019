﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Most
{
    public class BG2
    {
        public void narysujLinie() => Console.WriteLine("Rysuję linię za pomocą biblioteki graficznej BG2");
        public void narysujOkrag() => Console.WriteLine("Rysuję okrąg za pomocą biblioteki graficznej BG2");
    }
}
