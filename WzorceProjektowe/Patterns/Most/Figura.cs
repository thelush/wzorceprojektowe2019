﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Most
{
    public abstract class Figura
    {
        protected Biblioteka biblioteka;
        public Biblioteka Biblioteka { set => biblioteka = value; }
        public abstract void rysuj();
        protected virtual void rysujLinie() => biblioteka.rysujLinie();
        protected virtual void rysujOkrag() => biblioteka.rysujOkrag();
    }
}
