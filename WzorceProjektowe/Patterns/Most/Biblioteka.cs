﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Most
{
    public abstract class Biblioteka
    {
        public abstract void rysujLinie();
        public abstract void rysujOkrag();
    }
}
