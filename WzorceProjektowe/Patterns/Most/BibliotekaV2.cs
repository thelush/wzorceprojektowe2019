﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Most
{
    public class BibliotekaV2 : Biblioteka
    {
        private BG2 _bg = new BG2();
        public override void rysujLinie() => _bg.narysujLinie();
        public override void rysujOkrag() => _bg.narysujOkrag();
    }
}
