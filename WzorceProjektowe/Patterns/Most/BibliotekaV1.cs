﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Most
{
    public class BibliotekaV1 : Biblioteka
    {
        private BG1 _bg = new BG1();
        public override void rysujLinie() => _bg.rysuj_linie();
        public override void rysujOkrag() => _bg.rysuj_okrag();
    }
}
