﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Proxy
{
    public class QuadraticEquationProxy : IQuadraticEquation
    {
        bool byloRozwiazanie = true;
        double preva, prevb, prevc, prevx1, prevx2;
        private QuadraticEquation qe = new QuadraticEquation();
        public void CalculateQuadEq(double a, double b, double c)
        {
            if (a == preva && b == prevb && c == prevc && byloRozwiazanie)
            {
                Console.WriteLine("Dane i rozwiazanie takie jak poprzednie wyswietlane z proxy");
                Console.WriteLine($"x1 = {prevx1}");
                Console.WriteLine($"x2 = {prevx2}");
            }
            else if (a == preva && b == prevb && c == prevc && !byloRozwiazanie)
            {
                Console.WriteLine("Dane i rozwiązanie takie jak poprzednie wyswietlane z proxy");
                Console.Write("Brak rozwiązania");
            }
            else
            {
                qe.CalculateQuadEq(a, b, c);
                preva = a;
                prevb = b;
                prevc = c;
                prevx1 = qe.x1;
                prevx2 = qe.x2;

                if (qe.brakRozwiazania)
                {
                    byloRozwiazanie = false;
                    Console.WriteLine("Brak rozwiązania");
                }
            }
        }
    }
}
