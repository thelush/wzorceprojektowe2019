﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Proxy
{
    public interface IQuadraticEquation
    {
        public void CalculateQuadEq(double a, double b, double c);
    }
}
