﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Proxy
{
    public class QuadraticEquation : IQuadraticEquation
    {
        public bool brakRozwiazania = false;
        public double d, x1, x2;
        public void CalculateQuadEq(double a, double b, double c)
        {
            d = (b * b) - 4 * a * c;
            if (d == 0)
            {
                x1 = -b / (2.0 * a);
                x2 = x1;
                Console.WriteLine($"x1 = {x1}");
                Console.WriteLine($"x2 = {x2}");
            }
            else if (d > 0)
            {
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
                x2 = (-b - Math.Sqrt(d)) / (2 * a);

                Console.WriteLine($"x1 = {x1}");
                Console.WriteLine($"x2 = {x2}");
            }
            else
            {
                brakRozwiazania = true;
            }
        }
    }
}
