﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public abstract class Komponent
    {
        public abstract string drukuj();
    }
}
