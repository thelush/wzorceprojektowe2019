﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class Zamowienie
    {
        public void drukuj(Konfiguracja.TypPotwierdzenia typ) => Console.WriteLine(new Konfiguracja().pobierzPotwierdzenie(typ));
    }
}
