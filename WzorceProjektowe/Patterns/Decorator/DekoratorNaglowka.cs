﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class DekoratorNaglowka : DekoratorPotwierdzenia
    {
        public DekoratorNaglowka(Komponent komponent) : base(komponent) { }
        public override string drukuj() => base.drukuj();
        public string drkNaglowek() => "NAGŁÓWEK 1";
    }
}
