﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class DekoratorPotwierdzenia : Komponent
    {
        private Komponent komponent;
        public DekoratorPotwierdzenia(Komponent komponent) => this.komponent = komponent;

        public override string drukuj()
        {
            if (this.komponent != null)
                return komponent.drukuj();
            else
                return string.Empty;
        }
    }
}
