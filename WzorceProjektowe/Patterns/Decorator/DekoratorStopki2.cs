﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class DekoratorStopki2 : DekoratorPotwierdzenia
    {
        public DekoratorStopki2(Komponent komponent) : base(komponent) { }
        public override string drukuj() => base.drukuj();
        public string drkStopka() => "STOPKA 2";
    }
}
