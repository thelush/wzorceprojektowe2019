﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class Potwierdzenie : Komponent
    {
        public override string drukuj() => "POTWIERDZENIE";
    }
}
