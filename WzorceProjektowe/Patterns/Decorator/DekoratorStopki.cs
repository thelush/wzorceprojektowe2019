﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class DekoratorStopki : DekoratorPotwierdzenia
    {
        public DekoratorStopki(Komponent komponent) : base(komponent) { }
        public override string drukuj() => base.drukuj();
        public string drkStopka() => "STOPKA 1";
    }
}
