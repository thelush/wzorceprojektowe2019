﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Decorator
{
    public class Konfiguracja
    {
        public string pobierzPotwierdzenie(TypPotwierdzenia typ)
        {
            var sb = new StringBuilder();
            var potwierdzenie = new Potwierdzenie();

            if (typ == TypPotwierdzenia.pierwsze)
            {
                sb.AppendLine(new DekoratorNaglowka(potwierdzenie).drkNaglowek());
                sb.AppendLine(potwierdzenie.drukuj());
                sb.AppendLine(new DekoratorStopki(potwierdzenie).drkStopka());
                sb.AppendLine(new DekoratorStopki2(potwierdzenie).drkStopka());
            }
            else
            {
                sb.AppendLine(new DekoratorNaglowka(potwierdzenie).drkNaglowek());
                sb.AppendLine(new DekoratorNaglowka2(potwierdzenie).drkNaglowek());
                sb.AppendLine(potwierdzenie.drukuj());
                sb.AppendLine(new DekoratorStopki2(potwierdzenie).drkStopka());
            }

            return sb.ToString();
        }

        public enum TypPotwierdzenia
        {
            pierwsze,
            drugie
        }
    }
}
