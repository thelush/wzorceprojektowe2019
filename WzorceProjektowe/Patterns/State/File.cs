﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.State
{
    public class File
    {
        private FileState _state;

        public void Open()
        {
            _state = new FileOpened();
            Console.WriteLine("Otwieram plik");
        }
        public void Close()
        {
            _state = new FileClosed();
            Console.WriteLine("Zamykam plik");
        }
        public void Read() => Console.WriteLine(_state.GetType() == typeof(FileClosed) ? "Plik zamknięty - nie mogę czytać z pliku!" : "Czytam z pliku");
        public void Write() => Console.WriteLine(_state.GetType() == typeof(FileClosed) ? "Plik zamknięty - nie mogę zapisać do pliku!" : "Zapisuję do pliku");
    }
}
