﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Adapter
{
    public class Figura : IFigura
    {
        public virtual void wyswietl() => Console.WriteLine("Klasa Figura");
    }
}
