﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Adapter
{
    interface IFigura
    {
        public void wyswietl();
    }
}
