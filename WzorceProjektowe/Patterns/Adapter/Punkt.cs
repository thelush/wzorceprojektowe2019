﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Adapter
{
    public class Punkt : Figura
    {
        public override void wyswietl() => Console.WriteLine("Klasa Punkt");
    }
}
