﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Adapter
{
    public class Okrag : Figura
    {
        XXOkrag xxokrag;

        public Okrag() => xxokrag = new XXOkrag();

        public override void wyswietl() => xxokrag.wyswietlaj();
    }
}
