﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace WzorceProjektowe.Patterns.Builder
{
    public class Converter2 : TextConverter
    {
        public override string ConvertTagged(string text) => Regex.Replace(text, "<[^>]*>", string.Empty).ToUpper();
        public override string ConvertNonTagged(char text) => text.ToString().ToLower();
        public override string ConvertParagraph(string text) => throw new NotImplementedException();
    }
}
