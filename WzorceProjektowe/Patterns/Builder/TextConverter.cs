﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Builder
{
    public abstract class TextConverter
    {
        public string Text { get; set; }
        public abstract string ConvertTagged(string character);
        public abstract string ConvertNonTagged(char text);
        public abstract string ConvertParagraph(string text);
    }
}
