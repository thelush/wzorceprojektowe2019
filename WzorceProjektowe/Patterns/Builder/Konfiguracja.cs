﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Builder
{
    public class Konfiguracja
    {
        public TextConverter Builder;
        public Konfiguracja(TextConverter textConverter) => Builder = textConverter;
    }
}
