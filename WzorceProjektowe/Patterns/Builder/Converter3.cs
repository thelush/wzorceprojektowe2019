﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Builder
{
    public class Converter3 : TextConverter
    {
        public override string ConvertTagged(string text)
        {
            var start = text.IndexOf(">") + 1;
            var stop = text.Substring(start, text.Length-start).IndexOf("<");
            return text.Replace(start, stop, text.Substring(start, stop).ToUpper());
        }
        public override string ConvertNonTagged(char text) => text.ToString();
        public override string ConvertParagraph(string text) => throw new NotImplementedException();
    }
}
