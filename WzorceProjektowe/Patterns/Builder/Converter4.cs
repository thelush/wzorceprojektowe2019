﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace WzorceProjektowe.Patterns.Builder
{
    public class Converter4 : TextConverter
    {
        public override string ConvertTagged(string text)
        {
            var open = text.Substring(0, text.IndexOf('>') + 1);
            var c = open.Substring(1, text.IndexOf('>') - 1);
            var openR = open.Replace('<', '{').Replace('>', '#');
            var newStr = text.Replace(open, openR).Replace($"</{c}>", "}");
            return newStr;
        }
        public override string ConvertNonTagged(char text) => text.ToString();
        public override string ConvertParagraph(string text) => throw new NotImplementedException();
    }
}
