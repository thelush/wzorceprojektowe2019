﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace WzorceProjektowe.Patterns.Builder
{
    public class HTMLReader
    {
        public void ParseHTML(string plikString, Konfiguracja konfiguracja)
        {
            var newStr = string.Empty;
            var tag = string.Empty;
            bool openedTag = false;
            bool secondTag = false;
            for (var i = 0; i < plikString.Length; i++)
            {
                if ((plikString[i] == '<'))
                {
                    tag += plikString[i];
                    if (openedTag)
                        secondTag = true;
                    else
                        openedTag = true;
                }
                else if (plikString[i] == '>' && openedTag)
                {
                    tag += plikString[i];
                    if (secondTag)
                    {
                        newStr += konfiguracja.Builder.ConvertTagged(tag);
                        tag = string.Empty;
                        openedTag = false;
                        secondTag = false;
                    }
                }
                else if (openedTag)
                {
                    tag += plikString[i];
                }
                else
                {
                    newStr += konfiguracja.Builder.ConvertNonTagged(plikString[i]);
                }
            }

            konfiguracja.Builder.Text = newStr;
        }
    }
}
