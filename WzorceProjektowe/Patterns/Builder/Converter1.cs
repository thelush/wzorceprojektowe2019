﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace WzorceProjektowe.Patterns.Builder
{
    public class Converter1 : TextConverter
    {
        public override string ConvertTagged(string text) => Regex.Replace(text, "<[^>]*>", string.Empty);
        public override string ConvertNonTagged(char text) => text.ToString();
        public override string ConvertParagraph(string text) => throw new NotImplementedException();
    }
}
