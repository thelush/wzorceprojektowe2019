﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public class CenyCzarnorynkoweVisitor : IVisitor
    {
        public double suma = 0;
        public void Visit(Zwierze zwierze) => suma += zwierze.cenaCzarnorynkowa;
    }
}
