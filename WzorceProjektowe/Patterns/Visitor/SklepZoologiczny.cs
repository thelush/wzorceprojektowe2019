﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public class SklepZoologiczny
    {
        private List<Zwierze> _zwierzeta = new List<Zwierze>();

        public void Attach(Zwierze zwierze) => _zwierzeta.Add(zwierze);
        public void Accept(IVisitor visitor) => _zwierzeta.ForEach(item => item.Accept(visitor));
    }
}
