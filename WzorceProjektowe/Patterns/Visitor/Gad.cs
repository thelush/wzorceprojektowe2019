﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public class Gad : Zwierze
    {
        public Gad(bool zdrowe, double cenaSklepowa, double cenaCzarnorynkowa) : base(zdrowe, cenaSklepowa, cenaCzarnorynkowa)
        {
        }
    }
}
