﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public class WeterynarzVisitor : IVisitor
    {
        public void Visit(Zwierze zwierze)
        {
            var sb = new StringBuilder();
            sb.Append($"Odwiedzane zwierze : {zwierze.GetType().Name}. Diagnoza: ");
            if (zwierze.zdrowe)
                sb.Append("zdrowe.");
            else
            {
                if ((zwierze.cenaCzarnorynkowa / 2) > zwierze.cenaSklepowa)
                    sb.Append(" choroba. Leczenie: hospitalizacja.");
                else
                    sb.Append(" choroba. Leczenie: antybiotyki, dieta.");
            }

            Console.WriteLine(sb.ToString());
        }
    }
}
