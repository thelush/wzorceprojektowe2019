﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public class Ryba : Zwierze
    {
        public Ryba(bool zdrowe, double cenaSklepowa, double cenaCzarnorynkowa) : base(zdrowe, cenaSklepowa, cenaCzarnorynkowa)
        {
        }
    }
}
