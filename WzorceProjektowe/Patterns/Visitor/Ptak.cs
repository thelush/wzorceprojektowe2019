﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public class Ptak : Zwierze
    {
        public Ptak(bool zdrowe, double cenaSklepowa, double cenaCzarnorynkowa) : base(zdrowe, cenaSklepowa, cenaCzarnorynkowa)
        {
        }
    }
}
