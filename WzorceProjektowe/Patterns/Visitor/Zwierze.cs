﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public abstract class Zwierze
    {
        public bool zdrowe;
        public double cenaSklepowa;
        public double cenaCzarnorynkowa;

        public Zwierze(bool zdrowe, double cenaSklepowa, double cenaCzarnorynkowa)
        {
            this.zdrowe = zdrowe;
            this.cenaSklepowa = cenaSklepowa;
            this.cenaCzarnorynkowa = cenaCzarnorynkowa;
        }

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
