﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Visitor
{
    public interface IVisitor
    {
        public void Visit(Zwierze zwierze);
    }
}
