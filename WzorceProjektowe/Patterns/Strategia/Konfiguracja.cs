﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Strategia
{
    public class Konfiguracja
    {
        public TypPodatku podatek;
        public Konfiguracja(TypPodatku podatek) => this.podatek = podatek;
    }

    public enum TypPodatku
    {
        polski,
        niemiecki
    }
}
