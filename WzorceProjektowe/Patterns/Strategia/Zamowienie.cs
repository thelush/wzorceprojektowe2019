﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Strategia
{
    public class Zamowienie
    {
        private List<double> _zamowienie = new List<double>();
        public Zamowienie(List<double> towary) => _zamowienie = towary;
        public void obliczPodatek(Konfiguracja konf)
        {
            if (konf.podatek == TypPodatku.polski)
                _zamowienie.ForEach(item => Console.WriteLine(new PodatekPolska().kwotaPodatku(item)));
            else
                _zamowienie.ForEach(item => Console.WriteLine(new PodatekNiemcy().kwotaPodatku(item)));
        }
    }
}
