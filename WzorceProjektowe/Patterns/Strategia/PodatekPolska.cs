﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Strategia
{
    public class PodatekPolska : ObliczPodatek
    {
        public override double kwotaPodatku(double cena) => cena + cena * 0.23;
    }
}
