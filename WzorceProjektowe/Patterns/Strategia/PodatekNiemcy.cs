﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Strategia
{
    public class PodatekNiemcy : ObliczPodatek
    {
        public override double kwotaPodatku(double cena) => cena + 1.57;
    }
}
