﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Strategia
{
    public abstract class ObliczPodatek
    {
        public abstract double kwotaPodatku(double cena);
    }
}
