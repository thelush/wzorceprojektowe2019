﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Singleton
{
    public sealed class Singleton
    {
        private static Singleton instance = null;

        private Singleton() => Console.WriteLine("Utworzone obiekt klasy Singleton");

        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                else
                {
                    Console.WriteLine("Instancja klasy została już utworzona!");
                }
                return instance;
            }
        }
    }
}
