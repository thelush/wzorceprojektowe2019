﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Composite
{
    public class Picture : Graphic
    {
        private List<Graphic> _graphics = new List<Graphic>();

        public override void Add(Graphic g) => _graphics.Add(g);

        public override void Draw() => _graphics.ForEach(g => g.Draw());

        public override Graphic GetChild(int c) => _graphics[c];

        public override void Remove(Graphic g) => _graphics.Remove(g);
    }
}
