﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Composite
{
    public class Rectangle : Graphic
    {
        public override void Add(Graphic g) => throw new NotImplementedException();

        public override void Draw() => Console.WriteLine("Rysuję prostokąt");

        public override Graphic GetChild(int c) => throw new NotImplementedException();

        public override void Remove(Graphic g) => throw new NotImplementedException();
    }
}
