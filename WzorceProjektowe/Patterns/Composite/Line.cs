﻿using System;

namespace WzorceProjektowe.Patterns.Composite
{
    public class Line : Graphic
    {
        public override void Add(Graphic g) => throw new NotImplementedException();

        public override void Draw() => Console.WriteLine("Rysuję linię");

        public override Graphic GetChild(int c) => throw new NotImplementedException();

        public override void Remove(Graphic g) => throw new NotImplementedException();
    }
}
