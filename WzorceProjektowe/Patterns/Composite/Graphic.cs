﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Composite
{
    public abstract class Graphic
    {
        public abstract void Draw();
        public abstract void Add(Graphic g);
        public abstract void Remove(Graphic g);
        public abstract Graphic GetChild(int c);
    }
}
