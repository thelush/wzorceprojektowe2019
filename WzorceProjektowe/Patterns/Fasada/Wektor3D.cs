﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Fasada
{
    public class Wektor3D
    {
        public double x { get; }
        public double y { get; }
        public double z { get; }
        public Wektor3D(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            Console.WriteLine(ToString());
        }

        public static Wektor3D operator +(Wektor3D a, Wektor3D b) => new Wektor3D(a.x + b.x, a.y + b.y, a.z + b.z);

        public override string ToString() => $"Wektor3D x:{x} y:{y} z:{z}";
    }
}
