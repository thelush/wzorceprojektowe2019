﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Fasada
{
    public class Wektor2D
    {
        public double x { get; }
        public double y { get; }
        public Wektor2D(double x, double y)
        {
            this.x = x;
            this.y = y;
            Console.WriteLine(ToString());
        }

        public static Wektor2D operator +(Wektor2D a, Wektor3D b) => new Wektor2D(a.x + b.x, a.y + b.y);

        public override string ToString() => $"Wektor2D x:{x} y:{y}";
    }
}
