﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.TemplateMethod
{
    public class ZapytanieOracle : SzablonZapytania
    {
        protected override string formatujConnect(string con) => $"{con};";

        protected override string formatujSelect(string sel) => $"{sel};";
    }
}
