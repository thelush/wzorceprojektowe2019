﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.TemplateMethod
{
    public class ZapytanieSQLServer : SzablonZapytania
    {
        protected override string formatujConnect(string con) => con;

        protected override string formatujSelect(string sel) => sel;
    }
}
