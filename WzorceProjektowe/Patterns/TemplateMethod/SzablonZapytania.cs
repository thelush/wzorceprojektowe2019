﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.TemplateMethod
{
    public abstract class SzablonZapytania
    {
        public void WykonajZapytanie(string nazwaBD, string specZapy)
        {
            var komendaBD = formatujConnect(nazwaBD);
            Console.WriteLine(komendaBD);
            komendaBD = formatujSelect(specZapy);
            Console.WriteLine(komendaBD);
        }
        protected abstract string formatujSelect(string sel);
        protected abstract string formatujConnect(string con);
    }
}
