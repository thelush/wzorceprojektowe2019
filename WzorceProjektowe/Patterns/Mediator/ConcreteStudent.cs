﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Mediator
{
    public class ConcreteStudent : Student
    {
        public ConcreteStudent(Mediator mediator) : base(mediator)
        {
        }
    }
}
