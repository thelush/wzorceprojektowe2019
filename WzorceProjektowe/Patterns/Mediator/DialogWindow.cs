﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Mediator
{
    public class DialogWindow : Mediator
    {
        public override void EnterMarks(Student student)
        {
            WyswietlMenu(student);
            while (Console.ReadKey().Key != ConsoleKey.NumPad4)
            {
                if (Console.ReadKey().Key == ConsoleKey.NumPad1 || Console.ReadKey().Key == ConsoleKey.NumPad2 || Console.ReadKey().Key == ConsoleKey.NumPad3)
                {
                    WyswietlMenu(student);
                    Licz(Console.ReadKey().Key, student);
                }
            }
            return;
        }

        private void WyswietlMenu(Student student)
        {
            Console.WriteLine($"1. Ćwiczenia zaliczone. {(student.zaliczoneCwiczenia ? "X" : "")}");
            Console.WriteLine($"2. Wykład (egzamin) zaliczony. {(student.zaliczonyWyklad ? "X" : "")}");
            Console.WriteLine($"3. Przedmiot zaliczony (wygrany konkurs). {(student.wygranyKonkurs ? "X" : "")}");
            Console.WriteLine("4. Wyjście z programu");
        }
        private void Licz(ConsoleKey key, Student student)
        {
            if (key == ConsoleKey.NumPad1)
            {
                student.zaliczoneCwiczenia = true;
            }
            else if (key == ConsoleKey.NumPad2)
            {
                student.zaliczonyWyklad = true;
                student.zaliczoneCwiczenia = true;
            }
            else if (key == ConsoleKey.NumPad3)
            {
                student.wygranyKonkurs = true;
                student.zaliczonyWyklad = true;
                student.zaliczoneCwiczenia = true;
            }
        }
    }
}
