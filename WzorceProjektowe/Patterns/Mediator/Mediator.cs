﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Mediator
{
    public abstract class Mediator
    {
        public abstract void EnterMarks(Student student);
    }
}
