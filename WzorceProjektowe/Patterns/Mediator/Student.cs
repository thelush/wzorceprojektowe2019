﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Mediator
{
    public abstract class Student
    {
        protected Mediator mediator;
        public bool zaliczoneCwiczenia = false;
        public bool zaliczonyWyklad = false;
        public bool wygranyKonkurs = false;
        public Student(Mediator mediator) => this.mediator = mediator;
        public void wprowadzOceny() => this.mediator.EnterMarks(this);
    }
}
