﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.FactoryMethod
{
    public class ZapytanieSQLServer : SzablonZapytania
    {
        public override BazaDanych utworzBD() => new BazaDanychSQLServer();
        protected override string formatujSelect(string sel) => sel;
    }
}
