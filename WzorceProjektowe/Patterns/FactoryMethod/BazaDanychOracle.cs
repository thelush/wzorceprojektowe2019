﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.FactoryMethod
{
    public class BazaDanychOracle : BazaDanych
    {
        public override void wykonajSelect(string sel) => Console.WriteLine($"{sel}\nZapytanie wysłane do bazy Oracle");
    }
}
