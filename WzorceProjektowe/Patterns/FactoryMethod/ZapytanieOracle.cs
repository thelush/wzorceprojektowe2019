﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.FactoryMethod
{
    public class ZapytanieOracle : SzablonZapytania
    {
        public override BazaDanych utworzBD() => new BazaDanychOracle();
        protected override string formatujSelect(string sel) => $"{sel};";
    }
}
