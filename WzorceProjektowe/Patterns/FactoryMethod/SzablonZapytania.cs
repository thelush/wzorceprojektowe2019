﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.FactoryMethod
{
    public abstract class SzablonZapytania
    {
        public void WykonajZapytanie(string nazwaBD, string specZapy)
        {
            var BD = utworzBD();
            var komendaBD = formatujSelect(specZapy);
            BD.wykonajSelect(komendaBD);
        }

        public abstract BazaDanych utworzBD();
        protected abstract string formatujSelect(string sel);
    }
}
