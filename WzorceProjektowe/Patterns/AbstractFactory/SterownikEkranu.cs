﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public abstract class SterownikEkranu
    {
        public void rysuj(SE se) => se.rysuj();
    }
}
