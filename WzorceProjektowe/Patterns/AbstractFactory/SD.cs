﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class SD
    {
        private ISD _sd;
        public SD(ISD sd) => _sd = sd;
        public void drukuj() => _sd.drukuj();
    }
}
