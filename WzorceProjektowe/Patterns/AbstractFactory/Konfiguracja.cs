﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class Konfiguracja
    {
        public Konfiguracja(Rozdzielczosc rozdz)
        {
            if (rozdz == Rozdzielczosc.niska)
                new ApNadzorujaca(new FabrykaNisRozdz());
            else
                new ApNadzorujaca(new FabrykaWysRozdz());
        }
    }

    public enum Rozdzielczosc
    {
        niska,
        wysoka
    }
}
