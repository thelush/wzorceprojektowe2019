﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class SE
    {
        private ISE _se;
        public SE(ISE se) => _se = se;
        public void rysuj() => _se.rysuj();
    }
}
