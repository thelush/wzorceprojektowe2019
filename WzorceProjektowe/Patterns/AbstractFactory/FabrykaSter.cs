﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public abstract class FabrykaSter
    {
        public abstract SterownikEkranu pobierzSterEkrn();
        public abstract SterownikDrukarki pobierzSterDruk();
    }
}
