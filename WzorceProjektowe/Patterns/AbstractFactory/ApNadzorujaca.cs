﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class ApNadzorujaca
    {
        public ApNadzorujaca(FabrykaSter fabryka)
        {
            var sterEkrn = fabryka.pobierzSterEkrn();
            var sterDruk = fabryka.pobierzSterDruk();
            sterEkrn.rysuj(new SE(new SENR()));
            sterDruk.drukuj(new SD(new SDNR()));
        }
    }
}
