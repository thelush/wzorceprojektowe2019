﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class FabrykaWysRozdz : FabrykaSter
    {
        public override SterownikEkranu pobierzSterEkrn() => new SterErknWysRozdz();
        public override SterownikDrukarki pobierzSterDruk() => new SterDrukWysRozdz();
    }
}
