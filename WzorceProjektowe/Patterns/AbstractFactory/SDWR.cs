﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class SDWR : ISD
    {
        public void drukuj() => Console.WriteLine("Drukuję figurę za pomocą sterownika drukarki wysokiej rozdzielczości");
    }
}
