﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class FabrykaNisRozdz : FabrykaSter
    {
        public override SterownikEkranu pobierzSterEkrn() => new SterEkrnNisRozdz();
        public override SterownikDrukarki pobierzSterDruk() => new SterDrukNisRozdz();
    }
}
