﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public abstract class SterownikDrukarki
    {
        public void drukuj(SD sd) => sd.drukuj();
    }
}
