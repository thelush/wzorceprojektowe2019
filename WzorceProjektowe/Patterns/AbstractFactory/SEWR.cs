﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.AbstractFactory
{
    public class SEWR : ISE
    {
        public void rysuj() => Console.WriteLine("Rysuję figurę za pomocą sterownika ekranu niskiej rozdzielczości");
    }
}
