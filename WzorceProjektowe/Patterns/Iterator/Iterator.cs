﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Iterator
{
    public class Iterator
    {
        List<decimal> lista;
        int position;
        public Iterator(List<decimal> lista)
        {
            this.lista = lista;
            this.position = -1;
        }

        public decimal CurrentItem => lista[position];
        public decimal First() => lista[0];
        public decimal Next() => lista[position];
        public bool IsDone() => ++position == lista.Count ? false : true;
    }
}
