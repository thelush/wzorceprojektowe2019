﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Iterator
{
    public class IteratorNonZero
    {
        List<decimal> lista = new List<decimal>();
        int position;
        public IteratorNonZero(List<decimal> lista)
        {
            getRidOfZeroValues(lista);
            this.position = -1;
        }

        public decimal CurrentItem => lista[position];
        public decimal First() => lista[0];
        public decimal Next() => lista[position];
        public bool IsDone() => ++position == lista.Count ? false : true;
        private void getRidOfZeroValues(List<decimal> listIn)
        {
            foreach (var item in listIn)
                if (item != 0)
                    lista.Add(item);
        }
    }
}
