﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.Iterator
{
    public class MyCollection
    {
        List<decimal> lista = new List<decimal>();
        public MyCollection(List<decimal> a) => lista.AddRange(a);

        public void Add(decimal a) => lista.Add(a);

        public Iterator GetIterator => new Iterator(lista);
        public IteratorNonZero GetIteratorNonZero => new IteratorNonZero(lista);
    }
}
