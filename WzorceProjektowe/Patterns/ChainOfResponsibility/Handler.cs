﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.ChainOfResponsibility
{
    public abstract class Handler
    {
        protected Handler successor;
        public void SetSuccessor(Handler successor) => this.successor = successor;
        public abstract void HandleHelp();
        public abstract void ShowHelp();
    }
}
