﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.ChainOfResponsibility
{
    public class Application : Handler
    {
        public override void HandleHelp()
        {
            Console.WriteLine($"Application: odebrałem żądanie wyświetlenia pomocy");
            ShowHelp();
        }
        public override void ShowHelp() => Console.WriteLine($"Application: wyświetlam pomoc");
    }
}
