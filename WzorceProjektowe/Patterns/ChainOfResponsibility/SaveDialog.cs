﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.ChainOfResponsibility
{
    public class SaveDialog : Handler
    {
        public override void HandleHelp()
        {
            Console.WriteLine($"SaveDialog: odebrałem żądanie wyświetlenia pomocy");
            ShowHelp();
            successor.HandleHelp();
        }
        public override void ShowHelp() => Console.WriteLine($"SaveDialog: nie potrafię obsłużyć żądania i przekazuję je dalej");
    }
}
