﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.ChainOfResponsibility
{
    public class OKButton : Handler
    {
        public override void HandleHelp()
        {
            Console.WriteLine($"OKButton: odebrałem żądanie wyświetlenia pomocy");
            ShowHelp();
            successor.HandleHelp();
        }
        public override void ShowHelp() => Console.WriteLine($"OKButton: nie potrafię obsłużyć żądania i przekazuję je dalej");
    }
}
