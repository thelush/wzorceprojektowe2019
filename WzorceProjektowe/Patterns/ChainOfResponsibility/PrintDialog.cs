﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.ChainOfResponsibility
{
    public class PrintDialog : Handler
    {
        public override void HandleHelp()
        {
            Console.WriteLine($"PrintDialog: odebrałem żądanie wyświetlenia pomocy");
            ShowHelp();
            successor.HandleHelp();
        }

        public override void ShowHelp() => Console.WriteLine($"PrintDialog: nie potrafię obsłużyć żądania i przekazuję je dalej");
    }
}
