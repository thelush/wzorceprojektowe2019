﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WzorceProjektowe.Patterns.ChainOfResponsibility
{
    public class PrintButton : Handler
    {
        public override void HandleHelp()
        {
            Console.WriteLine($"PrintButton: odebrałem żądanie wyświetlenia pomocy");
            ShowHelp();
            successor.HandleHelp();
        }
        public override void ShowHelp() => Console.WriteLine($"PrintButton: nie potrafię obsłużyć żądania i przekazuję je dalej");
    }
}
